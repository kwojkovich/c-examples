#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int is_prime(int i) {
    if(i <= 3) {
     return i > 1;
    }
    else if(i % 2 == 0 || i % 3 == 0) {
     return 0;
    }
    else {
      int n = 5;
      while(n * n <= i) {
         if(i % n == 0 || i % (n + 2) == 0) {
          return 0;
         }
         n += 6;
      }
      return 1;
    }
}

int main() {
  int i = 0;
  for(i = INT_MAX; i <= INT_MAX; i++) {
    int result = 0;
    result = is_prime(i);
    if(result == 1) {
     printf("%d \t", i);
    }
  }
}
