# C Examples
This is a collection of C programs that explore some fundamentals in computer science.
I am working through these in order to strengthen my knowledge and to develop my skillset
around testing systems.

My goal is to prepare some of these examples to run on an emulated mainframe via Hercules and the TK-4 project.

## Disclaimer
These examples are by no means rigorous or should be used for anything other than personal research and tinkering.
