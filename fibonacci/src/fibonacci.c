#include <stdlib.h>
#include <stdio.h>

int fib(int n)
{
    if (n < 2) {
        return n;
    } else {
        return fib(n - 1) + fib(n - 2);
    }
}

void display(int n, int arr[])
{
    int i;
    for (i = 0; i <= n; i++) {
        if (i > 1 && i % 10 == 0) {
            printf("\n");
        }
        printf("%d\t", arr[i]);
    }
    printf("\n");
}

// Print Fibonacci numbers until 19
int fib_main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("You must specify a number as an argument.\n");
        return 1;
    }

    int n;
    n = atoi(argv[1]);

    if (n < 0) {
        printf("You must enter a number greater than 0.");
    }

    int *arr = malloc(sizeof(int) * (n + 1));
    int i;
    for (i = 0; i <= n; i++) {
        arr[i] = fib(i);
    }
    display(n, arr);
    return 0;
}
