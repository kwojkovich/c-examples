#include "CUnit/Basic.h"
#include "../../src/fibonacci.c"

void test_sequence_0(void)
{
    int result = fib(0);
    CU_ASSERT(0 == result);
}

void test_sequence_1(void)
{
    int result = fib(1);
    CU_ASSERT(1 == result);
}

void test_sequence_2(void)
{
    int result = fib(2);
    CU_ASSERT(1 == result);
}

void test_sequence_3(void)
{
    int result = fib(3);
    CU_ASSERT(2 == result);
}

void test_sequence_4(void)
{
    int result = fib(4);
    CU_ASSERT(3 == result);
}

void test_sequence_10(void)
{
    int result = fib(10);
    CU_ASSERT(55 == result);
}
