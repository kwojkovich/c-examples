// Main Includes
#include <stdio.h>
#include <stdlib.h>
#include "CUnit/Basic.h"

// Include Tests
#include "test_cases.h"

void main(void)
{
    CU_initialize_registry();

    // Create the test suite
    CU_pSuite suite = CU_add_suite("Fibonacci", 0, 0);

    // Add tests to the test suite
    CU_add_test(suite, "test 0", test_sequence_0);
    CU_add_test(suite, "test 1", test_sequence_1);
    CU_add_test(suite, "test 2", test_sequence_2);
    CU_add_test(suite, "test 3", test_sequence_3);
    CU_add_test(suite, "test 4", test_sequence_4);
    CU_add_test(suite, "test 10", test_sequence_10);

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
}
