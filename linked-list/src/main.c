#include <stdio.h>
#include <stdlib.h>

struct Node
{
 int key;
 struct Node *next;
};

struct Node *cursor = NULL;

void addNodeExp(int key, struct Node *HeadNode) {
  struct Node *cursor = NULL;

  // Allocate new Node
  struct Node *node = (struct Node*) malloc(sizeof(struct Node));
  node->key = key;

  // This is a really naieve way to code this
  // Start with the head node
  cursor =  HeadNode;
  // Move the pointer to the last element
  while(cursor->next != NULL) {
    cursor = cursor->next;
  }
  cursor->next = node;
}

struct Node * addNode(int key, struct Node *current) {
  // Allocate new Node
  struct Node *node = (struct Node*) malloc(sizeof(struct Node));
  node->key = key;
  current->next = node;
  return node;
}

void printList(struct Node *HeadNode) {
  struct Node *printer = NULL;

  printer = HeadNode;
  while(printer->next != NULL) {
    printf("Address: %p Key: %d, Next Key: %d \n", 
     (void *)printer, printer->key, printer->next->key
    );
    printer = printer->next;
  }
  printf("Address: %p Key: %d Next Key: None.\n", (void *)printer, printer->key);
}

int main(int argc, char *argv[]) {
  int i = 0;
  int items = 0;


  scanf("%d", &items);
  printf("Executing for %d items.\n", items);

  // Create first node in list
  struct Node *head = (struct Node*) malloc(sizeof(struct Node));
  head->key = 1;
  head->next = NULL;
  cursor = head;

  for(i = 2; i <= items; i++) {
   cursor = addNode(i,cursor);
  }

  printList(head);

  return 0;
}

